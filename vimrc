" --------------------------------------------------------------
" VUNDLE SETUP
" --------------------------------------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" --------------------------------------------------------------
" PLUGINS BEGIN
" --------------------------------------------------------------
Plugin 'sjl/vitality.vim'
Plugin 'chriskempson/base16-vim'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'kien/ctrlp.vim'
Plugin 'hecal3/vim-leader-guide'
Plugin 'scrooloose/nerdcommenter'
Plugin 'rust-lang/rust.vim'
Plugin 'Chiel92/vim-autoformat'
Plugin 'tpope/vim-surround'
Plugin 'scrooloose/syntastic'
Plugin 'rhysd/vim-clang-format'
Plugin 'osyo-manga/vim-over'
Plugin 'matze/vim-move'
Plugin 'jansenm/vim-cmake'
Plugin 'mrtazz/DoxygenToolkit.vim'
Plugin 'justinmk/vim-sneak'
Plugin 'szw/vim-maximizer'
Plugin 'tpope/vim-sensible'
Plugin 'mileszs/ack.vim'
Plugin 'wellle/targets.vim'
Plugin 'christoomey/vim-tmux-navigator'
Plugin 'tpope/vim-commentary'

" --------------------------------------------------------------
" PLUGINS END
" --------------------------------------------------------------
call vundle#end()            " required
filetype plugin indent on    " required

syntax on
set background=dark
"colorscheme base16-ocean
" colorscheme crunchbang

" Load project-specific vimrc files
set exrc
set secure

" Hide buffers when switching
set hidden

" hide backups and swap files
set backupdir=/tmp
set directory=/tmp

" 4-space tabs setup by default
set tabstop=4
set softtabstop=4
set expandtab
set shiftwidth=4
set smarttab
set cino=N-s " don't indent namespaces
set cino+=:0
set cino+=g0

" Line numbering
set number
set relativenumber
autocmd InsertEnter * :set number
autocmd InsertLeave * :set relativenumber

set hlsearch

" Make ESC clear search highlight
nnoremap <esc> :nohl<return><esc>

" Stop auto-commenting
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" C-a and C-e go to beginning and end of line in insert mode
inoremap <C-e> <C-o>$
inoremap <C-a> <C-o>^

" Make Y act like C and D (yank to end of line)
noremap Y y$

" Stay in visual mode when indenting. You will never have to run gv after
" performing an indentation.
vnoremap < <gv
vnoremap > >gv

" indentLine settings
let g:indentLine_char = '┊'
let g:indentLine_leadingSpaceChar = '·'
let g:indentLine_leadingSpaceEnabled = 1
let g:indentLine_color_term = 0

" airline settings
" disable whitespace checking
let g:airline#extensions#whitespace#enabled = 0
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

" Git Gutter settings
highlight SignColumn ctermbg=0
highlight GitGutterAdd ctermbg=0
highlight GitGutterAdd ctermfg=64
highlight GitGutterDelete ctermbg=0
highlight GitGutterDelete ctermfg=red
highlight GitGutterChange ctermbg=0
highlight GitGutterChange ctermfg=100

" Tell Racer where to find the Rust source
let g:ycm_rust_src_path="~/code/rust_src/src"

" Make clipboard work
set clipboard=unnamed

" Don't cache Ctrl-P
let g:ctrlp_use_caching = 0
let g:ctrlp_custom_ignore = 'build'

let g:DoxygenToolkit_commentType = 'C++'

let g:sneak#streak = 1

" OSL syntax
autocmd BufRead,BufNew,BufNewFile,BufCreate *.osl set filetype=osl

" Format buffers on write
"au BufWrite,FileType *.cpp,*.hpp,*.h,*.c,*.cu,*.cuh :Autoformat
" Use clang-format for CUDA
let g:formatters_cuda = ['clangformat']

" --------------------------------------------------------------
" KEY BINDINGS
" --------------------------------------------------------------
" Window motions
nmap <silent> <C-k> :wincmd k<CR>
nmap <silent> <C-j> :wincmd j<CR>
nmap <silent> <C-h> :wincmd h<CR>
nmap <silent> <C-l> :wincmd l<CR>

nnoremap <C-Left> :bp<CR>
nnoremap <C-Right> :bn<CR>

let mapleader = "\<Space>"

nnoremap <Esc><Esc> :pc<CR>

" ------------------
" COMPLETE PARAMETER
" ------------------
"inoremap <silent><expr> ( complete_parameter#pre_complete("()")
"smap <c-j> <Plug>(complete_parameter#goto_next_parameter)
"imap <c-j> <Plug>(complete_parameter#goto_next_parameter)
"smap <c-k> <Plug>(complete_parameter#goto_previous_parameter)
"imap <c-k> <Plug>(complete_parameter#goto_previous_parameter)

" --------------------------------------------------------------
" LEADER KEYS
" --------------------------------------------------------------
let g:lmap = {}

"nnoremap <Leader>o :CtrlP<CR>

nnoremap <Leader><Tab> :b#<CR>

let g:ackprg = 'ag --vimgrep'
nnoremap <Leader>a :Ack!<Space>

" If you use NERDCommenter:
let g:lmap.c = { 'name' : 'Comments' }
" " Define some descriptions
let g:lmap.c.c = ['call feedkeys("\<Plug>NERDCommenterComment")','Comment']
let g:lmap.c[' '] = ['call feedkeys("\<Plug>NERDCommenterToggle")','Toggle']
let g:lmap.c.d = ['Dox', 'Doxygen']

let g:lmap.p = {
            \'name': 'CtrlP',
            \'f': ['CtrlP', 'Files'],
            \'r': ['CtrlPMRU', 'Recent'],
            \}

let g:lmap.v = {
            \'name': 'Vim config',
            \'v': ['edit $MYVIMRC', 'Edit vimrc'],
            \'r': ['source $MYVIMRC', 'Reload vimrc'],
            \}

let g:lmap.f = {
            \'name': 'Files',
            \'s': ['s', 'Write'],
            \}

let g:lmap.b = {
            \'name': 'Buffers',
            \'b': ['CtrlPBuffer', 'Find'],
            \'n': ['bn', 'Next'],
            \'p': ['bp', 'Previous'],
            \'d': ['bd', 'Close'],
            \'w': ['w', 'Write'],
            \'f': ['Autoformat', 'Format'],
            \}

let g:lmap.y = {
            \'name': 'YCM',
            \'g': ['YcmCompleter GoTo', 'GoTo'],
            \'i': ['YcmCompleter GoToInclude', 'GoToInclude'],
            \'t': ['YcmCompleter GetType', 'Type'],
            \'d': ['YcmCompleter GetDoc', 'Doc'],
            \'R': ['YcmRestartServer', 'Restart'],
            \}

let g:lmap.w = {
            \'name': 'Window',
            \'w': ['w', 'Write'],
            \'s': ['split', 'Horizontal split'],
            \'v': ['vsplit', 'Vertical split'],
            \'c': ['close', 'Close'],
            \'m': ['MaximizerToggle', 'Toggle maximizer'],
            \}

call leaderGuide#register_prefix_descriptions("<Space>", "g:lmap")
nnoremap <silent> <leader> :LeaderGuide '<Space>'<CR>
vnoremap <silent> <leader> :LeaderGuideVisual '<Space>'<CR>
